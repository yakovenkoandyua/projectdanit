let sectionStandard = document.getElementById('standard');
let sectionUltimate = document.getElementById('ultimate');
let sectionPremium = document.getElementById('premium');
let table = document.getElementById('table');
let buyNowFirst = document.getElementById('buy-now1');
let buyNowSecond = document.getElementById('buy-now2');
let buyNowThree = document.getElementById('buy-now3');
function changeThemLocal() {
    const changeTheme = document.createElement('button');
    document.querySelector('table').before(changeTheme);
    changeTheme.classList.add('btn');
    changeTheme.innerText = 'change theme';
    changeTheme.addEventListener('click', () => {
        sectionStandard.classList.toggle('section-standard');
        sectionUltimate.classList.toggle('section-ultimate');
        sectionPremium.classList.toggle('section-premium');
        table.classList.toggle('table');
        buyNowFirst.classList.toggle('section-standard');
        buyNowSecond.classList.toggle('section-ultimate');
        buyNowThree.classList.toggle('section-premium');
        if(localStorage.getItem('Theme') === null){
            localStorage.setItem('Theme', 'change bgc')
        } else {
            localStorage.removeItem('Theme')
        }
    });
    window.addEventListener('load', () => {
        if(localStorage.getItem('Theme') !== null){
            sectionStandard.classList.toggle('section-standard');
            sectionUltimate.classList.toggle('section-ultimate');
            sectionPremium.classList.toggle('section-premium');
            table.classList.toggle('table');
            buyNowFirst.classList.toggle('section-standard');
            buyNowSecond.classList.toggle('section-ultimate');
            buyNowThree.classList.toggle('section-premium');
        }
    });
}
changeThemLocal();
