/*** GAME CREATION ***/
const container = document.getElementById('container');
const btnCreateGame = document.getElementById('create-game-btn');

btnCreateGame.addEventListener('click', () => {
    const whackGame = new WhackGame(container);
    whackGame.render(container);
    btnCreateGame.classList.add('hidden')
});

/*** CLASS CONSTRUCTOR WhackGame ***/
class WhackGame {
    constructor(level, player1Name = 'Anonymous', player2Name = 'Computer') {
        this.person = {
            name: player1Name,
            score: 0,
        };
        this.computer = {
            name: player2Name,
            score: 0,
        };
        this.fieldSideSize = WhackGame.FIELD_SIZE.min;
        this.scoreToWin = WhackGame.SCORE_TO_WIN.min;
        this.winner = '';
    };

    static get LEVEL() {
        return {
            EASY: 1500,
            HARD: 1000,
            PROFESSIONAL: 500,
        }
    };

    static get FIELD_SIZE() {
        return {
            min: 5,
            max: 10,
        }
    };

    static get SCORE_TO_WIN() {
        return {
            min: 3,
            max: 50,
        }
    };

    render(container) {

        container.innerHTML = `
  <div class="game-wrapper">
    <div class="game-setup">
      <div class="game-input" id="field-size-block">
        <label class="game-input-title" id="field-size-title" for="field-size-value"><strong>Field
          Size</strong><br>(from 5 to 10)</label>
        <input type="number" min="5" max="10" class="game-input switchable" id="field-size-value"
               placeholder="">
      </div>
      <div class="game-input" id="score-to-win-block">
        <label class="game-input-title" id="score-to-win-title"
               for="score-to-win-value"><strong>Score To Win</strong><br>(from 3 to 50)</label>
        <input type="number" min="3" max="50" class="game-input switchable" id="score-to-win-value"
               placeholder="">
      </div>
    </div>
    <div class="game-level-settings">
      <button class="btn btn-level switchable chosen" id="btn-light">EASY</button>
      <button class="btn btn-level switchable" id="btn-hard">HARD</button>
      <button class="btn btn-level switchable" id="btn-professional">PROFESSIONAL</button>
    </div>
    <button class="btn switchable" id="btn-game-start">START NEW GAME</button>
    <div class="score-section-wrapper">
      <div class="score-block" id="score-block-person">
        <span class="score-title" id="score-title-person">Person</span>
        <span class="score-value" id="score-value-person"></span>
      </div>
      <h3 class="score-section-title">SCORE</h3>
      <div class="score-block" id="score-block-computer">
        <span class="score-title" id="score-title-computer">Computer</span>
        <span class="score-value" id="score-value-computer"></span>
      </div>
    </div>
    <table class="field" id="game-field">
    </table>
  </div>`;

        this.renderField();
        this.setGameLevel();

        /*** GAME START ***/
        const btnStartNewGame = document.getElementById('btn-game-start');
        btnStartNewGame.onclick = () => {
            this.renderField();
            this.person.score = 0;
            this.computer.score = 0;
            this.winner = '';

            const fieldSize = +document.getElementById('field-size-value').value;
            const scoreToWin = +document.getElementById('score-to-win-value').value;

            this.fieldSideSize = (fieldSize < WhackGame.FIELD_SIZE.min
                || fieldSize > WhackGame.FIELD_SIZE.max) ? WhackGame.FIELD_SIZE.min : fieldSize;

            this.scoreToWin = (scoreToWin < WhackGame.SCORE_TO_WIN.min
                || scoreToWin > WhackGame.SCORE_TO_WIN.max) ? WhackGame.SCORE_TO_WIN.min : scoreToWin;

            const cellArray = document.querySelectorAll(".field-cell");
            cellArray.forEach(el => el.classList.remove('red', 'blue', 'green'));

            this.play();
        }
    };

    renderField() {
        const gameField = document.getElementById('game-field');
        gameField.innerHTML = '';
        for (let i = 0; i < this.fieldSideSize; i++) {
            const fieldRow = document.createElement('tr');
            fieldRow.classList.add('field-row');
            gameField.append(fieldRow);
            for (let j = 0; j < this.fieldSideSize; j++) {
                const fieldCell = document.createElement('th');
                fieldCell.classList.add('field-cell');
                fieldRow.append(fieldCell);
            }
        }
    }

    setPlayer1Name() {
        const player1Name = prompt('Enter your name:', 'Anonymous');
        return !player1Name ? 'Anonymous' : player1Name;
    }

    setGameLevel() {
        /*** GAME LEVEL SETUP***/
        const gameLevelButtons = document.querySelectorAll('.btn-level');
        gameLevelButtons.forEach(el => {
            el.addEventListener('click', (e) => {
                gameLevelButtons.forEach(el => el.classList.remove('chosen'));
                e.target.classList.add('chosen');
                this.level = e.target.textContent;
            })
        });
    };

    getGameLevel() {
        const level = document.querySelector('.chosen');
        return WhackGame.LEVEL[level.textContent];
    }

    play() {
        this.renderField();
        this.levelTime = this.getGameLevel();
        this.person.name = this.setPlayer1Name();

        /*** PRINT PLAYER NAMES AND STARTING SCORE ***/
        this.person.scoreEl = document.getElementById('score-value-person');
        this.person.nameEl = document.getElementById('score-title-person');
        this.computer.scoreEl = document.getElementById('score-value-computer');
        this.computer.nameEl = document.getElementById('score-title-computer');

        this.person.scoreEl.textContent = this.person.score;
        this.person.nameEl.textContent = this.person.name;
        this.computer.scoreEl.textContent = this.computer.score;
        this.computer.nameEl.textContent = this.computer.name;

        const btnAndInputArray = document.querySelectorAll('.switchable');
        this.disableElements(btnAndInputArray);

        const firstMoveTimer = setTimeout(this.makeNextMove.bind(this), this.levelTime);
    };

    makeNextMove() {
        const freeCellArray = document.querySelectorAll("th:not(.blue)");
        const cellIndex = Math.floor((freeCellArray.length - 1) * Math.random());
        const currentCell = freeCellArray[cellIndex];
        currentCell.classList.add('blue');
        currentCell.onclick = (e) => this.scoresPerson(currentCell, e);
        setTimeout(this.watchCell.bind(this, currentCell), this.levelTime);
    };

    scoresPerson(currentCell, e) {
        e.target.classList.add('green');
        currentCell.onclick = null;
        this.person.scoreEl.textContent = ++this.person.score;
        this.checkWinner(this.person.score, this.computer.score, 'person');
    }

    watchCell(currentCell) {
        if (currentCell.classList.contains('green')) {
            !this.winner ? this.makeNextMove() : "";
            return;
        }
        currentCell.onclick = null;
        currentCell.classList.add('red');
        this.computer.scoreEl.textContent = ++this.computer.score;
        this.checkWinner(this.person.score, this.computer.score, 'computer');
        !this.winner ? this.makeNextMove() : "";
    }

    checkWinner(personScore, computerScore, id) {
        const playerScore = id === 'person' ? personScore : computerScore;
        if (playerScore === this.scoreToWin) {
            this.winner = this[id].name;
            setTimeout(() => alert(`Congratulations! ${this[id].name} wins!`),);
            const btnAndInputArray = document.querySelectorAll('.switchable');
            this.enableElements(btnAndInputArray);
            return;
        }
    };

    disableElements(btnAndInputArray) {
        btnAndInputArray.forEach(el => el.disabled = true);
    };

    enableElements(btnAndInputArray) {
        btnAndInputArray.forEach(el => el.disabled = false);
    };
}

