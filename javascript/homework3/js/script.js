function calc(firstNumber, secondNumber, operation) {
    firstNumber = prompt("Enter first number", '4');
    while (isNaN(firstNumber) || firstNumber === '' || firstNumber === null) {
        firstNumber = prompt("Enter first number again")
    }
    secondNumber = prompt("Enter second number", '5');
    while (isNaN(secondNumber) || secondNumber === '' || secondNumber === null) {
        secondNumber = prompt("Enter second number again")
    }
    operation = prompt("Enter symbol operation", '+');
    while (operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/') {
        operation = prompt("Enter symbol operation again")
    }
    switch (operation) {
        case '+':
            result = +(firstNumber) + +(secondNumber);
            break;
        case '-':
            result = firstNumber - secondNumber;
            break;
        case '*':
            result = firstNumber * secondNumber;
            break;
        case '/':
            result = firstNumber / secondNumber;
            break;
    } return result;
}
console.log(calc());