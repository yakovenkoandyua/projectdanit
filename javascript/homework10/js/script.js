const eyeOpen = document.querySelector('.fa-eye');
eyeOpen.addEventListener('click', () => {
    if (eyeOpen.classList.contains('fa-eye')) {
        eyeOpen.classList.replace('fa-eye', 'fa-eye-slash');
        document.getElementsByClassName('input')[0].type = 'text';
    } else {
        eyeOpen.classList.replace('fa-eye-slash', 'fa-eye');
        document.getElementsByClassName('input')[0].type = 'password'
    }
});
const eyeClose = document.querySelector('.fa-eye-slash');
eyeClose.addEventListener('click', () => {
    if (eyeClose.classList.contains('fa-eye-slash')){
        eyeClose.classList.replace('fa-eye-slash','fa-eye');
        document.getElementsByClassName('input')[1].type = 'password';
    } else {
        eyeClose.classList.replace('fa-eye','fa-eye-slash');
        document.getElementsByClassName('input')[1].type = 'text';
    }
});
const confirmBtn = document.getElementById('confirm-btn');
const passwordInput = document.getElementsByTagName('input');
confirmBtn.addEventListener('click', (event) => {
    event.preventDefault();
    if (passwordInput[0].value === passwordInput[1].value) {
        alert(`You are welcome`);
    } else {
        const errorText = document.querySelector('#text') || document.createElement('span');
        errorText.id = 'text';
        errorText.style.color = 'red';
        errorText.style.marginBottom = '10px';
        errorText.innerText = 'Нужно ввести одинаковые значения';
        confirmBtn.before(errorText);
    }
});
