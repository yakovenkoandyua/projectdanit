function tab() {
     let tabNav = document.querySelectorAll('.tabs-title');
     let tabContent = document.querySelectorAll('.tab');
     let tabName;
     tabNav.forEach(item => {
         item.addEventListener('click', selectTabNav)
     });
     function selectTabNav() {
         tabNav.forEach(item => {
             item.classList.remove('active');
             });
         this.classList.add('active');
         tabName = this.getAttribute('data-tab-name');
         selectTabContent(tabName)
     }
     function selectTabContent(tabName) {
         tabContent.forEach(item => {
             item.classList[item.classList.contains(tabName) ? 'add' : 'remove']('active');
         })
     }
 }

tab();
