const input = document.getElementById('number');
const divPrice = document.getElementById('price');
input.onfocus = function () {
    input.classList.add('focus');
    if (this.classList.contains('invalid')){
        this.classList.remove('invalid');
        error.innerHTML = '';
    }
};
input.onblur = function () {
    if (input.value <= 0){
        input.classList.add('invalid');
        error.innerHTML = 'Please enter correct price.';
        return;
    }
    const textPrice = document.createElement('span'); /*document.querySelector('#textprice') || */
    // textPrice.id = 'textprice';
    textPrice.innerText = `Текущая цена` + input.value;
    divPrice.appendChild(textPrice);
    const buttonX = document.createElement('button'); /*document.querySelector('#buttonEsc') || скорей всего что это условие не обязательно, потому что нужно вводить несколько раз, но если ввести числа и несколько раз убрать фокус, то создаются новые спаны */
    // buttonX.id = 'buttonEsc';
    buttonX.innerText = `x`;
    buttonX.classList.add('price-esc');
    textPrice.appendChild(buttonX);
    input.style.color = 'green';
    buttonX.addEventListener('click', () => {
        textPrice.hidden = true;
        buttonX.hidden = true;
        input.value = '';
    })
};

