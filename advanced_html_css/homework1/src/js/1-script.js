$('.header__navbar-menu').on('click', () => {
    $('.header__navbar-list').slideToggle();
    $('.header__navbar-menu').toggleClass('open');
});

$('.header__navbar-menu').on('touch', () => {
    $('.header__navbar-list').slideToggle();
});

