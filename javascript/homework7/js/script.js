let listArray = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const showArray = function(array){
    const ul = document.createElement("ul");
    let newArray = array.map((el) => {
        const li = document.createElement("li");
        li.innerText = el;
        return li;
    });
    newArray.forEach((el) => {
        ul.appendChild(el);
    });
    console.log(newArray);
    console.log(listArray);
    return ul;
};
document.querySelector('script').before(showArray(listArray));