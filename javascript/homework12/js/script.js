let imgContent = document.querySelectorAll('.image-to-show');
let currentImg = 0;
let slider = setInterval(imgSlider, 5000);
function imgSlider() {
    imgContent[currentImg].classList.remove('current-img');
    currentImg = (currentImg + 1) % imgContent.length;
    imgContent[currentImg].classList.add('current-img');
}
const btnContainer = document.createElement('div');
btnContainer.classList.add('btn-wrapper');
document.querySelector("script").before(btnContainer);
const stopTimeOut = document.createElement('button');
stopTimeOut.classList.add('btn');
stopTimeOut.innerText = 'Прекратить показ';
    btnContainer.appendChild(stopTimeOut);
    stopTimeOut.addEventListener('click', () => {
        clearInterval(slider);
    });
const resumeTimeOut = document.createElement('button');
resumeTimeOut.classList.add('btn');
resumeTimeOut.innerText = 'Возобновить показ';
btnContainer.appendChild(resumeTimeOut);
document.querySelector('script').before(btnContainer);
    resumeTimeOut.addEventListener('click', () => {
       slider = setInterval(imgSlider, 5000)
    });

// let timer = document.getElementById('timer');
// window.onload = function(){
//     let msSecond = 1000;
//     let second = 5;
//     setInterval(function(){
//         timer.innerHTML = second+ ":" + msSecond;
//         msSecond = msSecond - 1000;
//         if(msSecond === 0){
//             second--;
//             msSecond = 1000;
//         }
//
//     }, 0.01);
// };

