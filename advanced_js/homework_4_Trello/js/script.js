class Trello {
    render(){
        const container = document.createElement('div');
        container.classList.add('container');
        const title = document.createElement('h1');
        title.innerText = 'To do';
        title.classList.add('title');
        const filter = document.createElement('a');
        filter.innerText = 'Filter card';
        filter.classList.add('filter-btn');
        const header = document.createElement('div');
        header.classList.add('header');
        const wrapper = document.createElement('div');
        wrapper.classList.add('wrapper');
        const button = document.createElement('a');
        button.classList.add('add-btn');
        button.innerText = 'Add card';
        button.id = 'myBtn';
        button.addEventListener('click', () => {

        });
        const addColumn = document.createElement('a');
        addColumn.classList.add('add-column');
        addColumn.innerText = 'Add Column';
        addColumn.addEventListener('click', () => {
            let tasks = document.createElement('div');
            tasks.classList.add('wrapper-tasks');
            wrapper.appendChild(tasks)
        });
        const column = document.createElement('div');
        column.classList.add('wrapper-tasks');
        wrapper.appendChild(column);
        header.append(title,filter,addColumn);
        container.append(header,wrapper,button);
        document.getElementsByTagName("script")[0].before(container)
    }
}

const trello = new Trello();
trello.render();

class Card {
    constructor(header,text,className){
        this._header = header;
        this._text = text;
        this._className = className;
    }

}

let modal = document.getElementById("myModal");
let btn = document.getElementById("myBtn");
let span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
    modal.classList.add('bg-active')
};
span.onclick = function() {
    modal.classList.remove('bg-active')
};
window.onclick = function(e) {
    if (e.target == modal) {
        modal.classList.remove('bg-active')
    }
};