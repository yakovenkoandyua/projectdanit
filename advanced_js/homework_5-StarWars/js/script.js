
const xhr = new XMLHttpRequest();
const ajax = new XMLHttpRequest();
ajax.open('GET', 'https://swapi.co/api/films/');
ajax.responseType = 'json';
ajax.send();
ajax.onload = function () {
    if (ajax.status != 200) {
        console.log(`Ошибка ${ajax.status}: ${ajax.statusText}`)
    } else {
        let {results: episodes} = ajax.response;
        const table = document.getElementById('table');
        let dataAttribute = 0;
        table.innerHTML = episodes.map(i =>
            `<li class="list-episode"><strong>Эпизод:</strong> ${i.title}<br />
             <strong>Номер Эпизода</strong> - ${i.episode_id}<br />
             <strong>Краткое описание</strong> - ${i.opening_crawl}</li> 
             <button class="btn-hero" data-number=${dataAttribute++}>Список персонажей</button>`).join('');
        table.addEventListener('click', function (e) {
            let target = e.target.getAttribute("data-number");
            const btnActiveList = document.getElementsByClassName('btn-hero');
            btnActiveList[target].remove();
            const nameHero = document.createElement('ul');
            const listEpisode = document.getElementsByClassName('list-episode');
            listEpisode[target].after(nameHero);
            episodes[target].characters.forEach(item => {
                xhr.open('GET', `${item}`, false);
                xhr.send();
                xhr.onload = function () {
                    if (xhr.status != 200) {
                        console.log(`Ошибка ${xhr.status}: ${xhr.statusText}`)
                    } else {
                        let result = JSON.parse(xhr.response);
                        const list = document.createElement('li');
                        list.innerText = `${result.name}`;
                        nameHero.appendChild(list)
                    }
                };
            })
        });
    }
    ajax.onerror = function () {
        alert("Запрос не удался");
    };
};
ajax.onprogress = function (event) {
    const loader = document.querySelector('.second-wrapper');
    if (event.lengthComputable) {
        loader.style.display = 'block';
    } else {
        loader.style.display = 'none';
    }
};