const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    babel = require('gulp-babel'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync').create();
const path = {
    prod: {
        html: 'prod',
        css: 'prod/css',
        js: 'prod/js',
        img: 'prod/img',
        self: 'prod'
    }  ,
    src: {
        html: 'src/*.html',
        scss: 'src/scss/*.scss',
        js: 'src/js/*.js',
        img: 'src/img/*.*'
    }
};
const htmlBuild = () => (
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.prod.html))
        .pipe(browserSync.stream())
);
const imgBuild = () => (
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.prod.img))
        .pipe(browserSync.stream())
);
const scssBuild = () => (
    gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
        .pipe(gulp.dest(path.prod.css))
        .pipe(browserSync.stream())
);
const jsBuild = () => (
    gulp.src(path.src.js)
        .pipe(concat('script.js'))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest(path.prod.js))
        .pipe(browserSync.stream())
);
const cleanProd = () => (
    gulp.src(path.prod.self, {allowEmpty: true})
        .pipe(clean())
);
const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./prod"
        }
    });

};
gulp.task('html', htmlBuild);
gulp.task('scss', scssBuild);
gulp.task('default', gulp.series(
    cleanProd,
    htmlBuild,
    scssBuild,
    imgBuild,
    jsBuild,
    watcher,
));