let $tabTitle = $('.tabs-title');
let $tabContent = $('.tab');

$tabTitle.on('click', (event) => {
   $('.active').removeClass('active');
   $('.active-content').removeClass('active-content');
   $(event.target).addClass('active');
   $tabContent.each((index, elem) => {
       if (event.target.dataset.name === elem.dataset.name) {
           $(elem).addClass('active-content');
       }
   })
   });
